package Logica;
public class ProductoNoRefrigerado extends Producto {

    public ProductoNoRefrigerado(String id, String nombre, double temperatura, double valorBase) {
        super(id, nombre, temperatura, valorBase);
    }

    public ProductoNoRefrigerado() {
        
    }

   // @Override // de la clase Producto esta el metodo
    public double calcularCostoDeAlmacenamiento() {
        double costo=getValorBase()*1.1;
        return costo;
    }

}
