package Logica;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import persistencia.ConexionBD;

public class Producto {

    private String nombre;
    private String id;
    private double temperatura;
    private double valorBase;

    public Producto(String nombre, String id, double temperatura, double valorBase) {
        this.nombre = nombre;
        this.id = id;
        this.temperatura = temperatura;
        this.valorBase = valorBase;
    }

    public Producto() {

    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public double getTemperatura() {
        return temperatura;
    }

    public void setTemperatura(double temperatura) {
        this.temperatura = temperatura;
    }

    public double getValorBase() {
        return valorBase;
    }

    public void setValorBase(double valorBase) {
        this.valorBase = valorBase;
    }

    @Override
    public String toString() {
        return "Producto{" + "nombre=" + nombre + ", id=" + id + ", temperatura=" + temperatura + ", valorBase=" + valorBase + '}';
    }

    public List<Producto> listartodo() { // Metodo para lista el producto registrado
        
        List<Producto> productos = new ArrayList<>(); 
        String sql;
         if("Consultar por id".equals(id)){
            sql = "SELECT * FROM Producto_reto5 ";
            
           }else{
        sql = "SELECT * FROM Producto_reto5 WHERE id=" + id + ""; // colocamos la sentencia para consulta a la BD en sql
         }
        ConexionBD conexion = new ConexionBD(); // creamos objeto de tipo conexion, se importa de la capa de persistencia del aplicativo nuestro
        try {  // try catch para capturar posible excepcion un error.
            ResultSet rs = conexion.consultarBD(sql); // crea un conjunto de resultados  envia el sql para consulta a la bd y se importa tambien de resulset
            while (rs.next()) {  // minestras tenga algo pro recorre haga esto operaciones para listar los productos
                Producto p; //lo declaramos ka variable del objeto
                p = new Producto(); //creamos el objeto inizializamos
                p. setId(rs.getString("id")); // seteamos los datos o inizializamos para obtener lo q venga como string en la columna id
                p.setNombre(rs.getString("nombre"));// seteamos para q nos traiga el nombre                
                p.setTemperatura(rs.getDouble("temperatura"));
                p.setValorBase(rs.getDouble("valorBase"));
                productos.add(p); // por ultimo se agrega

            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());// imprime el error
        } finally {
            conexion.cerrarConexion();  // cerramos la conexion siempre se ejecuta independiente si se ahce o no
        }
        return productos; // retornamos productos
    }

    // gaurdar producctos metodo
    public boolean registrarProducto() {
        boolean exito = false;
        ConexionBD conexion = new ConexionBD();
        String sql = "INSERT INTO Producto_reto5 (nombre,temperatura,valorBase)"
                + "VALUES('" + nombre + "','" + temperatura + "','" + valorBase + "')";
        if (conexion.setAutoCommitBD(false)) { // autocommit en false significa q no confirma cambios automaticos, lo ahcemos nostros
            if (conexion.insertarBD(sql)) { // si se conecta inserta
                conexion.commitBD(); // si todo esta bien hace commit a la bd
                exito = true; // le decimos q la conexion tuvo exito
                conexion.cerrarConexion();// siempre se debe cerrar la conexion
            } else {  // si no se le ahce un roollback y cerramos la conexion
                conexion.rollbackBD();
                conexion.cerrarConexion();
            }
        } else {
            conexion.cerrarConexion();  // si no logra conectarse se cierra la conexion
        }

        return exito;
    }
    // funcion para eliminar registros

    public boolean eliminarProducto() { // metodo para elimianr un producto
        boolean exito = false;
        ConexionBD conexion = new ConexionBD();
        String sql = "DELETE FROM Producto_reto5 WHERE id=" + id + "";

        if (conexion.setAutoCommitBD(false)) {  // autocomint en falso es para que no confirme en automatico y espera que uno confire el cambio
            if (conexion.actualizarBD(sql)) { // if else le decimos a la conexion q realize la conexion a la base de datos 
                conexion.commitBD();
                exito = true;
                conexion.cerrarConexion();
            }
        } else {
            conexion.rollbackBD();
            conexion.cerrarConexion();
        }
        return exito;
    }

    /// funcion para actualizar
    public boolean actualizar() {
        boolean exito = false;
        ConexionBD conexion = new ConexionBD();
        String sql = "UPDATE Producto_reto5 SET nombre='" + nombre + "', " + "temperatura='" + temperatura
                + "',valorBase=" + valorBase + ""
                + " WHERE id=" + id + ";";
        if (conexion.setAutoCommitBD(false)) {  // autocomint en falso es para que no confirme en automatico y espera que uno confire el cambio
            if (conexion.actualizarBD(sql)) { // if else le decimos a la conexion q realize la conexion a la base de datos 
                conexion.commitBD();
                exito = true;
                conexion.cerrarConexion();
            }
        } else {
            conexion.rollbackBD();
            conexion.cerrarConexion();
        }
        return exito;
    }
    /// la colocque para que calculara si era refrigerado o no refrigerado, el lio es que no supe como hacer que se fuera para tal metodo o para el otro
    public double calcularCosto()  {
      double costo = 0;
        if (getTemperatura()>=22){  
            
          //  ProductoNoRefrigerado p=new ProductoNoRefrigerado();
           // p.calcularCostoDeAlmacenamiento();
          //  calcularCostoDeAlmacenamiento();
            costo =  getValorBase()*1.1;
             
        }else{        
             
             costo = getValorBase()*1.2; 
        }
         return costo;
    }
  
}
