package Logica;
public class ProductoRefrigerado extends Producto {

    public ProductoRefrigerado(String id, String nombre, double temperatura, double valorBase) {
        super(id, nombre, temperatura, valorBase);
    }

    public ProductoRefrigerado() {
    }

     @Override
    public double calcularCostoDeAlmacenamiento() {
        return getValorBase()*1.2;
    }

}